require 'net/http'
require 'timeout'
timeout_in_seconds = 5 * 60 # 5min

begin
  Timeout.timeout(timeout_in_seconds) do
    while true
      response = nil
      Net::HTTP.start('gitlab.com', 80) {|http|
        response = http.head('/')
      }
      puts response.code
    end
  end
rescue
  #noop
end
